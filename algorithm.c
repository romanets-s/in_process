/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithm.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sromanet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/02 19:09:02 by sromanet          #+#    #+#             */
/*   Updated: 2017/08/02 19:09:42 by sromanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	algorithm(t_lem *lem)
{
	bfs_init(lem);
	bfs(lem, lem->start);
	print_way(lem, lem->end, -1);
	revers_int(lem);
	lem->way = create_int(lem, 0, ++lem->n_way);
	if (lem->out[1] == lem->end)
		lem->error = 1;
	while (lem->out[1] != lem->end)
	{
		block_room(lem);
		bfs_init(lem);
		bfs(lem, lem->start);
		print_way(lem, lem->end, -1);
		revers_int(lem);
		lem->way = create_int(lem, 1, ++lem->n_way);
	}
}

void	bfs_init(t_lem *lem)
{
	if (lem->used != NULL)
		free(lem->used);
	if (lem->dist != NULL)
		free(lem->dist);
	if (lem->parent != NULL)
		free(lem->parent);
	if (lem->queue != NULL)
		free(lem->queue);
	if (lem->out != NULL)
		free(lem->out);
	lem->used = new_int(lem->n_rooms, 0);
	lem->dist = new_int(lem->n_rooms, 0);
	lem->parent = new_int(lem->n_rooms, 0);
	lem->queue = new_int(lem->n_rooms, -1);
	lem->out = new_int(lem->n_rooms, -1);
}

void	bfs(t_lem *lem, int u)
{
	int i;

	lem->used[u] = 1;
	lem->parent[u] = u;
	lem->dist[u] = 1;
	queue_push(lem, u);
	while (lem->queue[0] != -1)
	{
		u = queue_front_pop(lem);
		i = -1;
		while (++i < lem->n_rooms)
		{
			if (!lem->used[i] && lem->connect[u][i] == 1)
			{
				lem->used[i] = 1;
				lem->parent[i] = u;
				lem->dist[i] = lem->dist[u] + 1;
				queue_push(lem, i);
			}
		}
	}
}

void	queue_push(t_lem *lem, int q)
{
	int i;

	i = lem->n_rooms;
	while (--i)
		lem->queue[i] = lem->queue[i - 1];
	lem->queue[i] = q;
}

int		queue_front_pop(t_lem *lem)
{
	int q;
	int i;

	q = lem->queue[0];
	i = -1;
	while (++i < lem->n_rooms - 1)
		lem->queue[i] = lem->queue[i + 1];
	return (q);
}
