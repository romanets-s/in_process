NAME = lem-in
SRC = main.c get_next_line.c split.c connect.c input.c rooms.c malloc_and_free.c algorithm.c algorithm2.c
SRCO = $(SRC:.c=.o)
INCLUDES = lem_in.h
FLAGS = -Wall -Wextra -Werror
LIBFT = libft/libft.a

all: $(NAME)

$(NAME): $(SRCO)
	@make -C libft
	@gcc $(FLAGS) -o $(NAME) $(SRCO) $(LIBFT)
	@echo "\033[32;1m<lem-in>   | done"

.c.o:
	@gcc $(FLAGS) -c $< -o $@

clean:
	@rm -f $(SRCO)
	@make clean -C libft
	@echo "\033[32;1m<lem-in>   | clean succes"

fclean: clean
	@rm -f $(NAME)
	@make fclean -C libft
	@echo "\033[32;1m<lem-in>   | fclean succes"

re: fclean all
	@echo "\033[32;1m<lem-in>   | re succes"
