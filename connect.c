/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   connect.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sromanet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/02 19:12:02 by sromanet          #+#    #+#             */
/*   Updated: 2017/08/02 19:12:04 by sromanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	connect(t_lem *lem, char *str, int i)
{
	char	**tmp;
	int		a;
	int		b;

	a = -1;
	b = -1;
	tmp = ft_strsplit(str, '-');
	if (ft_strsplit_len(tmp) == 2)
	{
		while (++i < lem->n_rooms)
		{
			if (ft_strequ(lem->rooms[i].name, tmp[0]))
				a = i;
			else if (ft_strequ(lem->rooms[i].name, tmp[1]))
				b = i;
		}
		if (a != -1 && b != -1)
		{
			lem->n_connect++;
			lem->connect[a][b] = 1;
			lem->connect[b][a] = 1;
		}
	}
	ft_strsplit_free(tmp);
}

void	next_step(t_lem *lem, int a, int n, int i)
{
	int		flag;

	flag = 0;
	while (++n < lem->n_way - 1)
	{
		i = 0;
		while (++i < lem->n_rooms)
		{
			if (lem->way[n][i] >= 0 && (lem->connect[0][lem->way[n][i]] == 0
			|| lem->way[n][i] == lem->end) && lem->ant[a] == lem->way[n][i - 1])
			{
				lem->connect[0][lem->way[n][i - 1]] -= 1;
				lem->connect[0][lem->way[n][i]] += 1;
				lem->ant[a] = lem->way[n][i];
				print_lem(a + 1, lem->rooms[lem->way[n][i]].name);
				flag = 1;
				break ;
			}
		}
		if (flag)
			break ;
	}
}

void	free_connect_tab(t_lem *lem, int i, int n)
{
	while (++i < lem->n_rooms)
	{
		n = -1;
		while (++n < lem->n_rooms)
		{
			if (i == 0 && n == lem->start)
				lem->connect[i][n] = lem->ants;
			else
				lem->connect[i][n] = 0;
		}
	}
}

void	block_way(t_lem *lem, int n)
{
	int i;

	i = -1;
	while (++i < lem->n_rooms)
		lem->way[n][i] = -42;
}

void	check_error(t_lem *lem, int n)
{
	while (++n < lem->n_way - 1)
	{
		if (lem->n_way > 2 && n > 0 && len_way(lem->way[n]) > lem->ants)
			block_way(lem, n);
	}
}
