/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithm2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sromanet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/02 19:11:05 by sromanet          #+#    #+#             */
/*   Updated: 2017/08/02 19:11:42 by sromanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	block_room(t_lem *lem)
{
	int i;
	int n;

	i = 0;
	while (++i < lem->n_rooms && lem->out[i] != lem->end)
	{
		n = -1;
		while (++n < lem->n_rooms)
		{
			lem->connect[n][lem->out[i]] = -42;
			lem->connect[lem->out[i]][n] = -42;
		}
	}
}

void	print_way(t_lem *lem, int end, int n)
{
	if (lem->parent[end] != end)
		print_way(lem, lem->parent[end], ++n);
	else
		n++;
	lem->out[n] = end;
}

void	revers_int(t_lem *lem)
{
	int *tmp;
	int i;
	int n;

	tmp = new_int(lem->n_rooms, -1);
	i = 0;
	while (lem->out[i] != -1)
		i++;
	n = -1;
	while (--i != -1)
		tmp[++n] = lem->out[i];
	if (lem->out != NULL)
		free(lem->out);
	lem->out = tmp;
}

int		*new_int(int n, int def)
{
	int *tmp;
	int i;

	tmp = (int *)malloc(sizeof(int) * n + 1);
	i = -1;
	while (++i <= n)
		tmp[i] = def;
	return (tmp);
}

int		len_way(int *way)
{
	int i;

	i = 0;
	while (way[i] != -1)
		++i;
	return (i);
}
