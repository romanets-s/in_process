/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sromanet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/02 19:08:17 by sromanet          #+#    #+#             */
/*   Updated: 2017/08/02 19:08:24 by sromanet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	start_finish(t_lem *lem, int a)
{
	int k;

	k = ft_strlen(lem->input);
	ft_putstr(lem->input + 1);
	ft_putstr("\n");
	if (lem->input[k - 1] != '\n')
		ft_putstr("\n");
	while (++a < lem->ants)
		print_lem(a + 1, lem->rooms[lem->end].name);
	ft_putstr("\n");
}

void	finish(t_lem *lem, int a)
{
	int k;

	k = ft_strlen(lem->input);
	ft_putstr(lem->input + 1);
	ft_putstr("\n");
	if (lem->input[k - 1] != '\n')
		ft_putstr("\n");
	while (lem->connect[0][lem->end] != lem->ants)
	{
		a = -1;
		while (++a < lem->ants)
			if (lem->ant[a] != lem->end)
				next_step(lem, a, -1, 0);
		ft_putstr("\n");
	}
}

int		error(t_lem *lem)
{
	if (lem->error == 1)
	{
		ft_putstr("ERROR\n");
		return (1);
	}
	else
		return (0);
}

int		main(void)
{
	t_lem	*lem;

	lem = create_s();
	input(lem);
	if (error(lem) == 1)
		return (EXIT_FAILURE);
	filling(lem, 0, 0);
	if (lem->error != 1 && lem->connect[lem->start][lem->end] == 1)
	{
		start_finish(lem, -1);
		return (EXIT_SUCCESS);
	}
	if (error(lem) == 1)
		return (EXIT_FAILURE);
	algorithm(lem);
	check_error(lem, -1);
	create_ant(lem, -1);
	free_connect_tab(lem, -1, -1);
	if (error(lem) == 1)
		return (EXIT_FAILURE);
	else
		finish(lem, 0);
	return (EXIT_SUCCESS);
}
